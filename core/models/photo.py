import random
import string

from io import BytesIO
from PIL import Image
from django.db import models
from django.core.files.base import ContentFile
from django.db.models.signals import post_delete

from ..mixins import UUIDMixin


def random_name(length):
    """
    Generate random name from letters and digits
    """
    return ''.join([
        random.choice(string.ascii_letters+string.digits)
        for i in range(length)
    ])


class Photo(UUIDMixin):
    """
    Photo in 3 resolutions.
    """
    MIDDLE_SIZE = (800, 800) # Long side length
    THUMBNAIL_SIDE = (150, 150)

    album = models.ForeignKey(
        'core.Album',
        on_delete=models.CASCADE,
        verbose_name="альбом",
        related_name='photos'
    )

    image_original = models.ImageField(
        upload_to='photos/originals'
    )

    image_middle = models.ImageField(
        upload_to='photos/middles',
        editable=False
    )

    image_thumbnail = models.ImageField(
        upload_to='photos/thumbnails',
        editable=False
    )

    added = models.DateTimeField(
        auto_now_add=True
    )


    class Meta:
        db_table = 'photos'
        ordering = ('added', )
        verbose_name = "фотография"
        verbose_name_plural = "фотографии"

    def __str__(self):
        return f'{self.album} [{self.added:%Y-%m-%d %H:%M:%S}]'

    def save(self, *a, **kw):
        """
        On the first save convert image 
        into 3 resolutions and save with random names
        """
        if not self.added:
            image = Image.open(self.image_original)
            io = BytesIO()
            image.save(io, 'JPEG', quality=100)
            self.image_original = ContentFile(
                io.getvalue(),
                f'{random_name(32)}.jpg'
            )
            
            image.thumbnail(self.MIDDLE_SIZE)
            io = BytesIO()
            image.save(io, 'JPEG', quality=100)
            self.image_middle = ContentFile(
                io.getvalue(),
                f'{random_name(32)}.jpg'
            )

            image.thumbnail(self.THUMBNAIL_SIDE)
            io = BytesIO()
            image.save(io, 'JPEG', quality=100)
            self.image_thumbnail = ContentFile(
                io.getvalue(),
                f'{random_name(32)}.jpg'
            )

        return super().save(*a, **kw)


# signal
def cleanup(sender, instance, **kw):
    """
    Remove files from storage for instances to be deleted
    """
    for field in sender._meta.get_fields():
        if isinstance(field, models.FileField):
            getattr(instance, field.name).delete(save=False)

post_delete.connect(receiver=cleanup, sender=Photo, dispatch_uid='Photo')

