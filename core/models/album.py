from django.db import models
from django.core.mail import send_mail
from django.conf import settings

from ..mixins import UUIDMixin
from .photo import Photo


class Album(UUIDMixin):
    """
    User's photo album.
    """
    user = models.ForeignKey(
        'core.User',
        on_delete=models.CASCADE,
        verbose_name="пользователь",
        related_name='albums'
    )

    title = models.CharField(
        "название",
        max_length=50
    )

    added = models.DateTimeField(
        auto_now_add=True
    )


    class Meta:
        db_table = 'albums'
        ordering = ('added', )
        verbose_name = "альбом"
        verbose_name_plural = "альбомы"

    def __str__(self):
        return f'{self.title} <{self.user}>'

    def create_photo(self, image_file):
        instance = Photo(
            album=self,
            image_original=image_file
        )
        instance.save()

        return instance

    def send_email(self, photo):
        email_data = {
            'subject': "Фотография добавлена в альбом",
            'message': f"В вашем альбоме \"{self.title}\" "
                       f"новая фотография (ID: {photo.id})",
            'from_email': settings.EMAIL_HOST_USER,
            'recipient_list': [self.user.email]
        }
        send_mail(**email_data)


