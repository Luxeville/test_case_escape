from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager

from ..mixins import UUIDMixin


__all__ = [
    'User',
]


class UserManager(BaseUserManager):
    """
    Manager for customized User.
    """
    use_in_migrations = True

    def create_user(self, email, password, **extra_fields):
        """
        Create a user with the given email and password.
        """
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        
        return user

    def create_superuser(self, email, password, **extra_fields):
        """
        Create a superuser with the given email and password.
        """
        extra_fields.update({'is_superuser': True})

        return self.create_user(email, password, **extra_fields)


class User(UUIDMixin, AbstractBaseUser):
    """
    User profile.
    Auth via email and password.
    """
    email = models.EmailField(
        "e-mail",
        unique=True
    )

    first_name = models.CharField(
        "имя",
        max_length=50
    )

    last_name = models.CharField(
        "фамилия",
        max_length=50
    )

    middle_name = models.CharField(
        "отчество",
        max_length=50,
        blank=True,
        default=''
    )

    birth_date = models.DateField(
        "дата рождения",
        blank=True,
        null=True
    )

    eye_color = models.ForeignKey(
        'catalog.EyeColor',
        on_delete=models.SET_NULL,
        verbose_name="цвет глаз",
        related_name='+',
        blank=True,
        null=True
    )

    country = models.ForeignKey(
        'catalog.Country',
        on_delete=models.SET_NULL,
        verbose_name="страна",
        related_name='+',
        blank=True,
        null=True
    )

    is_superuser = models.BooleanField(
        default=False,
        editable=False
    )


    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    objects = UserManager()


    class Meta:
        db_table = 'users'
        verbose_name = 'пользователь'
        verbose_name_plural = 'пользователи'

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    # Admin-site access methods and properties

    def has_perm(self, obj=None):
        return self.is_superuser

    def has_module_perms(self, app_label):
        return self.is_superuser

    @property
    def is_staff(self):
        return self.is_superuser





