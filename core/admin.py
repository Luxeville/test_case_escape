from django.contrib import admin
from django.contrib.auth import password_validation
from django.contrib.auth.models import Group
from django import forms

from .models import User, Album, Photo


# Groups are not used in the project
admin.site.unregister(Group)


class UserCreationForm(forms.ModelForm):

    password1 = forms.CharField(
        label="Пароль",
        strip=False,
        widget=forms.widgets.PasswordInput
    )

    password2 = forms.CharField(
        label="Пароль (еще раз)",
        strip=False,
        widget=forms.widgets.PasswordInput
    )

    class Meta:
        model = User
        fields = (
            'email',
            'password1',
            'password2',
            'first_name',
            'last_name',
            'middle_name',
            'birth_date',
            'eye_color',
            'country',
        )

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Пароли не совпадают")
        return password2

    def _post_clean(self):
        super()._post_clean()
        # Validate the password after self.instance is updated
        # with form data by super().
        password = self.cleaned_data.get('password1')
        if password:
            try:
                password_validation.validate_password(password, self.instance)
            except forms.ValidationError as error:
                self.add_error('password1', error)

    def save(self, *a, **kw):
        self.instance.set_password(self.cleaned_data['password1'])
        return super().save(*a, **kw)


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    
    readonly_fields = (
        'id',
        'password',
        'last_login',
    )

    def get_form(self, request, obj=None, **kw):
        defaults = {}
        if obj is None:
            defaults['form'] = UserCreationForm
        defaults.update(kw)

        return super().get_form(request, obj, **defaults)


@admin.register(Album)
class AlbumAdmin(admin.ModelAdmin):
    
    readonly_fields = (
        'id',
        'added',
    )


@admin.register(Photo)
class PhotoAdmin(admin.ModelAdmin):
    
    readonly_fields = (
        'image_middle',
        'image_thumbnail',
        'id',
        'added',
    )
