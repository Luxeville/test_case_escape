from rest_framework import serializers
from rest_framework.reverse import reverse

from catalog.models import Country, EyeColor
from catalog.serializers import CountrySerializer
from .models import User, Album, Photo


class PhotoHyperlinkField(serializers.HyperlinkedRelatedField):
    """
    API hyperlink for Photo (through Album)
    """
    view_name = 'photo-detail'

    def get_url(self, obj, view_name, request, format):
        url_kwargs = {
            'album_id': obj.album_id,
            'pk': obj.id
        }
        return reverse(view_name, kwargs=url_kwargs, request=request, format=format)

    def get_object(self, view_name, view_args, view_kwargs):
        lookup_kwargs = {
           'album_id': view_kwargs['album_id'],
           'pk': view_kwargs['pk']
        }
        return self.get_queryset().get(**lookup_kwargs)


class PhotoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Photo
        exclude = ()

    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        if self.instance:
            self.fields['image_original'].read_only = True


class AlbumSerializer(serializers.ModelSerializer):

    photos = PhotoHyperlinkField(
        many=True,
        read_only=True,
    )

    class Meta:
        model = Album
        fields = (
            'user',
            'id',
            'title',
            'added',
            'photos',
        )

    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        if self.instance:
            self.fields['user'].read_only = True


class UserSerializer(serializers.ModelSerializer):

    albums = serializers.HyperlinkedRelatedField(
        many=True,
        view_name='album-detail',
        read_only=True
    )

    class Meta:
        model = User
        fields = (
            'id',
            'first_name',
            'last_name',
            'middle_name',
            'birth_date',
            'email',
            'password',
            'eye_color',
            'country',
            'albums',
        )

    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        if self.instance:
            self.fields['password'].read_only = True

    def create(self, validated_data):
        user = User(**validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user
