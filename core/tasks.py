from uuid import UUID

from celery import shared_task, chain

from .models import Album, Photo


@shared_task
def create_photo(album_id, image_file):
	album = Album.objects.get(id=album_id)
	photo = album.create_photo(image_file)

	return (album.id, photo.id)


@shared_task
def send_email(album_photo):
	album_id, photo_id = album_photo
	album = Album.objects.get(id=album_id)
	photo = Photo.objects.get(id=photo_id)
	album.send_email(photo)


def create_photo_and_send_email(album_id, image_file):
	"""
	Asyncronously run chain of tasks
	"""
	task_chain = chain(
		create_photo.s(album_id, image_file),
		send_email.s()
	)
	task_chain.delay()