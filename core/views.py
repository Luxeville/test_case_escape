from uuid import UUID

from rest_framework import viewsets

from .models import User, Album, Photo
from .serializers import UserSerializer, AlbumSerializer, PhotoSerializer
from .tasks import create_photo_and_send_email


class UserViewSet(viewsets.ModelViewSet):

    queryset = User.objects.all()
    serializer_class = UserSerializer


class AlbumViewSet(viewsets.ModelViewSet):

    queryset = Album.objects.all()
    serializer_class = AlbumSerializer

    def get_queryset(self):
        """
        Filter albums by user with query param
        """
        raw_id = self.request.query_params.get('user')
        if raw_id:
            try:
                user_id = UUID(raw_id)
                return Album.objects.filter(user_id=user_id)

            except ValueError:
                return Album.objects.none()

        return Album.objects.all()


class PhotoViewSet(viewsets.ModelViewSet):

    queryset = Photo.objects.all()
    serializer_class = PhotoSerializer

    def get_queryset(self):
        """
        Filter by album (url part)
        """
        raw_id = self.kwargs.get('album_id')
        if raw_id:
            try:
                album_id = UUID(raw_id)
                return Photo.objects.filter(album_id=album_id)
            
            except ValueError:
                return Photo.objects.none()

        return Photo.objects.all()

    def perform_create(self, serializer):
        """
        Save photo asyncronously
        """
        request = serializer.context['request']
        temp_file = request.FILES['image_original'].temporary_file_path()
        album = serializer.data.get('album') 

        create_photo_and_send_email(album, temp_file)


