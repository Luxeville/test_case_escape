from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import UserViewSet, AlbumViewSet, PhotoViewSet


router = DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'albums', AlbumViewSet, 'album')
router.register(r'albums/(?P<album_id>[^/.]+)/photos', PhotoViewSet, 'photo')


urlpatterns = [
    path('', include(router.urls)),
]