import uuid

from django.db import models


class UUIDMixin(models.Model):
    """
    Mixin for models to use UUID as primary key.

    Note: uuid4 is used. Since it is randomly generated key,
    a collision possibility is very-very low, but non-zero.
    This issue is not solved in this test-case,
    but must be taken into account in production mode.
    """
    id = models.UUIDField(
        primary_key=True,
        editable=False,
        default=uuid.uuid4
    )

    class Meta:
        abstract = True