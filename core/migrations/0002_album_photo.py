# Generated by Django 2.2.1 on 2019-05-31 12:52

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Album',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=50, verbose_name='название')),
                ('added', models.DateTimeField(auto_now_add=True)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='albums', to=settings.AUTH_USER_MODEL, verbose_name='пользователь')),
            ],
            options={
                'verbose_name': 'альбом',
                'verbose_name_plural': 'альбомы',
                'db_table': 'albums',
                'ordering': ('added',),
            },
        ),
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('image_original', models.ImageField(upload_to='photos/originals')),
                ('image_middle', models.ImageField(editable=False, upload_to='photos/middles')),
                ('image_thumbnail', models.ImageField(editable=False, upload_to='photos/thumbnails')),
                ('added', models.DateTimeField(auto_now_add=True)),
                ('album', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='photos', to='core.Album', verbose_name='альбом')),
            ],
            options={
                'verbose_name': 'фотография',
                'verbose_name_plural': 'фотографии',
                'db_table': 'photos',
                'ordering': ('added',),
            },
        ),
    ]
