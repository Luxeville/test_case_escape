from django.contrib import admin

from .models import Country, EyeColor


class CatalogAdmin(admin.ModelAdmin):

	readonly_fields = (
		'id',
	)

admin.site.register(Country, CatalogAdmin)
admin.site.register(EyeColor, CatalogAdmin)
