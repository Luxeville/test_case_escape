import uuid

from django.db import models


__all__ = [
	'Country',
	'EyeColor',
]


class BaseCatalogModel(models.Model):
	"""
	Base mixin for catalog models
	"""
	id = models.UUIDField(
		primary_key=True,
		editable=False,
		default=uuid.uuid4
	)

	title = models.CharField(
		"название",
		max_length=50
	)

	description = models.TextField(
		"описание",
		blank=True,
		default=''
	)


	class Meta:
		abstract = True

	def __str__(self):
		return self.title


class Country(BaseCatalogModel):

	class Meta:
		verbose_name = "страна"
		verbose_name_plural = "страны"


class EyeColor(BaseCatalogModel):

	class Meta:
		verbose_name = "цвет глаз"
		verbose_name_plural = "цвета глаз"

