from rest_framework import serializers

from .models import Country, EyeColor


class CountrySerializer(serializers.ModelSerializer):

	class Meta:
		model = Country
		exclude = ()


class EyeColorSerializer(serializers.ModelSerializer):

	class Meta:
		model = EyeColor
		exclude = ()
