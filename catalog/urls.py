from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import CountryViewSet, EyeColorViewSet


router = DefaultRouter()
router.register('countries', CountryViewSet)
router.register('eye-colors', EyeColorViewSet)


urlpatterns = [
	path('', include(router.urls)),
]