from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator
from rest_framework import viewsets, permissions

from .models import Country, EyeColor
from .serializers import CountrySerializer, EyeColorSerializer


class CountryViewSet(viewsets.ModelViewSet):

	queryset = Country.objects.all()
	serializer_class = CountrySerializer

	@method_decorator(cache_page(60*60))
	def list(self, *a, **kw):
		return super().list(*a, **kw)


class EyeColorViewSet(viewsets.ModelViewSet):

	queryset = EyeColor.objects.all()
	serializer_class = EyeColorSerializer

	@method_decorator(cache_page(60*60))
	def list(self, *a, **kw):
		return super().list(*a, **kw)